import express from 'express';
import {Paths} from './constants/paths';
import {routerCreator} from './router';
import bodyParser from 'body-parser';
import MongoClient from 'mongodb';

const appHostname = 'localhost';
const appPort = 3000;

const mongoHostname = 'localhost';
const mongoPort = 27017;

MongoClient.connect(`mongodb://${mongoHostname}:${mongoPort}`, (err, database) => {

  if (err) return console.log(err);
  const db = database.db('moderntube');

  const app = express();
  app.use(bodyParser.urlencoded({extended:true}));
  app.use(bodyParser.json());
  app.use('', routerCreator(db));
  app.use((req, res) => res.sendStatus(404));

  app.listen(appPort, appHostname, () => {
    console.log(`ModernTube running at: http://${appHostname}:${appPort}${Paths.homepage}`);
  });

});
