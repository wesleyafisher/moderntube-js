import express from 'express';
import {Paths} from './constants/paths';

import {homepageController} from './controllers/homepageController';
import {loginController} from './controllers/loginController';
import {registerController} from './controllers/registerController';

const root = __dirname + '/../';

export const routerCreator = (db) => {
  const router = express.Router();

  router.route(Paths.homepage)
    .get(homepageController(root).get);

  router.route(Paths.loginpage)
    .get(loginController(root).get)
    .post(loginController(root, db).post);

  router.route(Paths.registerpage)
    .get(registerController(root).get)
    .post(registerController(root, db).post);

  return router;
};
