export const homepageController = (root) => {

  const get = (req, res, next) => {
    res.status(200).sendFile('client/index.html', {root});
  };

  return {
    get,
  };
};
