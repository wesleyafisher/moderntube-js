export const registerController = (root, db) => {

  const get = (req, res, next) => {
    res.status(200).sendFile('client/register.html', {root});
  };

  const post = (req, res, next) => {
    if (!req.body || !req.body.username || !req.body.password1 || !req.body.password2) {
      res.status(400).send('Bad request: Required information not provided');
      return;
    }
    if (req.body.password1 !== req.body.password2) {
      res.status(400).send('Bad request: Passwords do not match');
      return;
    }
    db.collection('users').insertOne( { _id: req.body.username, password: req.body.password1 },
      (err, result) => {
          if (err) res.status(400).send(err);
          else res.status(201).send(result);
    });
  };

  return {
    get,
    post,
  };
};
