export const loginController = (root, db) => {

  const get = (req, res, next) => {
    res.status(200).sendFile('client/login.html', {root});
  };

  const post = (req, res, next) => {
    if (!req.body || !req.body.username || !req.body.password) {
      res.status(400).send('Bad request: Required information not provided');
      return;
    }
    db.collection('users').find( { _id: req.body.username, password: req.body.password } )
      .toArray((err, result) => {
        if (err) res.status(400).send(err);
        else if (!result.length) res.status(400).send('Username and password not found');
        else res.status(201).send(result);
    });
  };

  return {
    get,
    post,
  };
};
